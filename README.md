# README

## Over view
This is the docker environment for python3.6.

## How to use this.
1.  Download this source file.
2. Yo use this command in the direction which has this docker-compose file.
    > docker-compose up -d --build
3. Conect to this docker container to use this command.
    > docker exec -it python3 bash
4. When you finished your work, dump it to use this command.
   > docker-compose down


## This env include this library
- pandas
- numpy
- matplotlib